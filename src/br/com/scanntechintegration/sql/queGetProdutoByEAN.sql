SELECT TOP 1 CODPROD
FROM TGFPRO
WHERE USOPROD <> 'S'
AND ISNULL(TIPGTINNFE, 0) <> 0
AND (
    (TIPGTINNFE = 1 AND CODPROD = :BGD_EAN)
    OR
    (TIPGTINNFE IN (2, 4) AND REFERENCIA = :STR_EAN)
    OR
    (TIPGTINNFE = 3 AND EXISTS ( SELECT 1 FROM TGFEST WHERE CODPROD = TGFPRO.CODPROD AND CODBARRA = :STR_EAN))
    OR
    (TIPGTINNFE = 4 AND EXISTS ( SELECT 1 FROM TGFVOA WHERE CODPROD = TGFPRO.CODPROD AND CODBARRA = :STR_EAN))
)