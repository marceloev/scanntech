package br.com.scanntechintegration.service;

import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;

import br.com.scanntechintegration.config.URLService;
import br.com.scanntechintegration.model.dto.CredentialsDTO;
import br.com.scanntechintegration.model.dto.WSCallerException;

public class WSCaller {

	private static final String HOST = "http://br.homo.apipdv.scanntech.com";
	private static final HttpClient HTTP_CLIENT = new HttpClient();
	private static final HttpClientParams HTTP_CLIENT_PARAMS = new HttpClientParams();

	static {
		HTTP_CLIENT.setParams(HTTP_CLIENT_PARAMS);
	}

	private final CredentialsDTO credentialsDTO;

	public WSCaller(CredentialsDTO credentialsDTO) {
		this.credentialsDTO = credentialsDTO;
	}

	public String executeGET(URLService url) throws WSCallerException {
		GetMethod getMethod = new GetMethod(url.getFinalURL(HOST, credentialsDTO));
		getMethod.setDoAuthentication(true);
		return execute(getMethod);
	}
	
	public InputStream executeInptGET(URLService url) throws WSCallerException {
		GetMethod getMethod = new GetMethod(url.getFinalURL(HOST, credentialsDTO));
		getMethod.setDoAuthentication(true);
		return executeInpt(getMethod);
	}

	public String executePOST(URLService url, RequestEntity body) throws WSCallerException {
		PostMethod postMethod = new PostMethod(url.getFinalURL(HOST, credentialsDTO));
		postMethod.setDoAuthentication(true);
		postMethod.setRequestEntity(body);
		return execute(postMethod);
	}

	private String execute(HttpMethod method) throws WSCallerException {
		HTTP_CLIENT.getState().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials(credentialsDTO.getUsername(), credentialsDTO.getPassword()));

		try {
			HTTP_CLIENT.executeMethod(method);

			if (method.getStatusCode() != 200) {
				throw new WSCallerException("Ocorreu um erro ao tentar buscar as promo��es!\n".concat(method.getResponseBodyAsString()), method.getStatusCode());
			} else {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			throw new WSCallerException(e.getMessage(), method.getStatusCode());
		}
	}
	
	private InputStream executeInpt(HttpMethod method) throws WSCallerException {
		HTTP_CLIENT.getState().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials(credentialsDTO.getUsername(), credentialsDTO.getPassword()));

		try {
			HTTP_CLIENT.executeMethod(method);

			if (method.getStatusCode() != 200) {
				throw new WSCallerException("Ocorreu um erro ao tentar buscar as promo��es!\n".concat(method.getResponseBodyAsString()), method.getStatusCode());
			} else {
				return method.getResponseBodyAsStream();
			}
		} catch (Exception e) {
			throw new WSCallerException(e.getMessage(), method.getStatusCode());
		}
	}

}
