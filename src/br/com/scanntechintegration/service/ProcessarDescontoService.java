package br.com.scanntechintegration.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.modelcore.dwfdata.listeners.tgf.DescontoListener;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.scanntechintegration.model.dto.BusinessException;
import br.com.scanntechintegration.model.dto.DescontoQuantidadeDTO;
import br.com.scanntechintegration.model.dto.PromocoesProdutoDTO;
import br.com.scanntechintegration.model.dto.PromocoesResultDTO;
import br.com.scanntechintegration.model.enums.TipoPromocao;
import br.com.scanntechintegration.sql.SQLLoader;
import br.com.scanntechintegration.utils.StringUtils;

public class ProcessarDescontoService {

	private Map<String, BigDecimal> produtosEan = new HashMap<String, BigDecimal>();

	public synchronized final void processarPromocoes(final List<PromocoesResultDTO> promocoes) throws Exception {
		SessionHandle hnd = null;
		try {
			hnd = JapeSession.open();
			hnd.setCanTimeout(false);

			hnd.execEnsuringTX(new JapeSession.TXBlock() {
				@Override
				public void doWithTx() throws Exception {
					
					EntityFacade dwf = null;
					JdbcWrapper jdbc = null;
					NativeSql queUpdPromo = null;
					NativeSql queInsDescPromo = null;
					NativeSql queInsDescQtd = null;
					NativeSql queGetProdByEAN = null;
					NativeSql queUpdDescGrupoProd = null;
					try {
						dwf = EntityFacadeFactory.getDWFFacade();
						jdbc = dwf.getJdbcWrapper();
						jdbc.openSession();
						jdbc.setQueryTimeout(0);
												
						queUpdPromo = new NativeSql(jdbc, SQLLoader.class, "queUpdItemIntegracao.sql");
						queUpdPromo.setReuseStatements(true);
						queUpdPromo.setBatchUpdateSize(500);
						
						queGetProdByEAN = new NativeSql(jdbc, SQLLoader.class, "queGetProdutoByEAN.sql");
						queGetProdByEAN.setReuseStatements(true);
						
						queUpdDescGrupoProd = new NativeSql(jdbc, SQLLoader.class, "queUpdateTGFPRO_GRUPODESCPROD.sql");
						queUpdDescGrupoProd.setReuseStatements(true);
						queUpdDescGrupoProd.setBatchUpdateSize(1000);
						
						queInsDescPromo = new NativeSql(jdbc, SQLLoader.class, "queInsertDescontoPromo.sql");
						queInsDescPromo.setReuseStatements(true);
						queInsDescPromo.setBatchUpdateSize(200);
					
						queInsDescQtd = new NativeSql(jdbc, SQLLoader.class, "queInsertDescQtd.sql");
						queInsDescQtd.setReuseStatements(true);
						queInsDescQtd.setBatchUpdateSize(50000);
						queInsDescQtd.setFillNamedParametersWithNull(true);
						
						for (PromocoesResultDTO promocao : promocoes) {
							
							StringBuilder msg = new StringBuilder();
							
							if (!promocao.isNotProcessed()) {
								continue;
							}
							
							if (promocao.getTipoPromocao() == TipoPromocao.ADICIONAL_REGALO) { //Brinde
								
							} else if (promocao.getTipoPromocao() == TipoPromocao.ADICIONAL_DESCUENTO) { // Desconto em Prod. Terceiro
								
							
							} else {
							
								try {								
									TipoPromocao tipoPromo = promocao.getTipoPromocao();
									BigDecimal nuPromocao = DescontoListener.getProximoNumeroPromocao(jdbc);
									String grupoDescProd = String.format("SCANNTECH:%s", nuPromocao.toString());
									
									msg.append("Processando promo��o com ID: " + nuPromocao +"\n");
									
									for (PromocoesProdutoDTO promocoesProdutoDTO : promocao.getDetalhes().getProdutos()) {
										BigDecimal codProduto = resolveCodProdByEAN(promocoesProdutoDTO.getEan(), queGetProdByEAN);
										promocoesProdutoDTO.setIdProd(codProduto);
										queUpdDescGrupoProd.setNamedParameter("GRUPODESCPROD", grupoDescProd);
										queUpdDescGrupoProd.setNamedParameter("CODPROD", codProduto);
										queUpdDescGrupoProd.addBatch();
										
										msg.append(String.format(" * Produto de c�digo %s marcado como Grupo de Desconto: %s\n", codProduto.toString(), grupoDescProd));
									}
									
									queInsDescPromo.setNamedParameter("DTINICIAL", promocao.getDhInicio());
									queInsDescPromo.setNamedParameter("DTFINAL", promocao.getDhFim());
									queInsDescPromo.setNamedParameter("DESCRPROMOCAO", promocao.getDescription());
									queInsDescPromo.setNamedParameter("LIQUIDACAO", "N");
									queInsDescPromo.setNamedParameter("NUPROMOCAO", nuPromocao);
									queInsDescPromo.setNamedParameter("GRUPODESCPROD", grupoDescProd);
									queInsDescPromo.setNamedParameter("CODPROD", BigDecimal.ZERO);
									queInsDescPromo.setNamedParameter("PERCENTUAL", promocao.getDetalhes().getPercDesconto(tipoPromo));
									queInsDescPromo.setNamedParameter("VLRDESC", promocao.getDetalhes().getVlrDesconto(tipoPromo));
									queInsDescPromo.setNamedParameter("USADESCQTD", promocao.getDetalhes().isUsaDescQtd(tipoPromo));
									queInsDescPromo.setNamedParameter("VLRVENDA", promocao.getDetalhes().getVlrVenda(tipoPromo));
									queInsDescPromo.setNamedParameter("CODTAB", promocao.getCodTab());
									queInsDescPromo.addBatch();
										
									for (DescontoQuantidadeDTO descQtdDTO : promocao.getDescontosQuantidade(nuPromocao)) {
										queInsDescQtd.setNamedParameter("NUPROMOCAO", nuPromocao);
										queInsDescQtd.setNamedParameter("QTDE", descQtdDTO.getQuantidade());
										queInsDescQtd.setNamedParameter("PERCDESC", descQtdDTO.getPercDesc());
										queInsDescQtd.setNamedParameter("DESCFIXO", descQtdDTO.getDescFixo());
										queInsDescQtd.setNamedParameter("CODUSU", BigDecimal.ZERO);
										queInsDescQtd.setNamedParameter("DTALTER", new Timestamp(System.currentTimeMillis()));
										queInsDescQtd.setNamedParameter("TIPDESC", descQtdDTO.getTipDesc());
										queInsDescQtd.addBatch();
									}
								
									queUpdDescGrupoProd.flushBatchTail();
									queInsDescPromo.flushBatchTail();
									queInsDescQtd.flushBatchTail();

									queUpdPromo.setNamedParameter("MSG", msg.toString());
									queUpdPromo.setNamedParameter("STATUSINT", "1");
									queUpdPromo.setNamedParameter("ID", promocao.getIdItem());
									queUpdPromo.setNamedParameter("CODEMP", promocao.getCodEmp());
									queUpdPromo.addBatch();

								} catch (Exception e) {
									if (e instanceof BusinessException) {
										msg.append(e.getMessage());
									} else {
										msg.append(StringUtils.convertThrowableToString(e));
									}
									queUpdPromo.setNamedParameter("MSG", msg.toString());
									queUpdPromo.setNamedParameter("STATUSINT", "-1");
									queUpdPromo.setNamedParameter("ID", promocao.getIdItem());
									queUpdPromo.setNamedParameter("CODEMP", promocao.getCodEmp());
									queUpdPromo.addBatch();
								}
							}
						}
						
						queUpdPromo.flushBatchTail();
					} finally {
						NativeSql.releaseResources(queGetProdByEAN);
						NativeSql.releaseResources(queUpdDescGrupoProd);
						NativeSql.releaseResources(queInsDescQtd);
						NativeSql.releaseResources(queInsDescPromo);
						NativeSql.releaseResources(queUpdPromo);
						JdbcWrapper.closeSession(jdbc);
						
					}
				}
			});
		} finally {
			JapeSession.close(hnd);
		}
	}
	
	private BigDecimal resolveCodProdByEAN(String ean, NativeSql queGetProdByEAN) throws Exception {
		BigDecimal codProd = produtosEan.get(ean);
		
		if (codProd == null) {
			ResultSet rsGetProdByEAN = null;
			try {
				
				queGetProdByEAN.setNamedParameter("BGD_EAN", BigDecimal.valueOf(Double.valueOf(ean)));
				queGetProdByEAN.setNamedParameter("STR_EAN", ean);
				rsGetProdByEAN = queGetProdByEAN.executeQuery();
				
				if (rsGetProdByEAN.next()) {
					codProd = rsGetProdByEAN.getBigDecimal("CODPROD");
					produtosEan.put(ean, codProd);
				}
			} finally {
				try {
					rsGetProdByEAN.close();
				} catch (Exception ignored) {}
			}
		}
		
		return codProd;
	}
}
