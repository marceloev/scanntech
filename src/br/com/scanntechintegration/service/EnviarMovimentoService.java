package br.com.scanntechintegration.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.core.JapeSession;
import br.com.sankhya.jape.core.JapeSession.SessionHandle;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.jape.vo.EntityVO;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.scanntechintegration.config.URLService;
import br.com.scanntechintegration.model.dto.CredentialsDTO;
import br.com.scanntechintegration.model.dto.FechaMovimentoDTO;
import br.com.scanntechintegration.model.dto.MovimentoVendaDTO;
import br.com.scanntechintegration.model.dto.MovimentoVendaFinanceiroDTO;
import br.com.scanntechintegration.model.dto.MovimentoVendaItemDTO;
import br.com.scanntechintegration.model.dto.WSCallerException;
import br.com.scanntechintegration.model.enums.TipoPagamento;
import br.com.scanntechintegration.utils.JsonUtils;
import br.com.scanntechintegration.utils.StringUtils;

public class EnviarMovimentoService extends Thread {

	public static final String ENVIO_MOVIMENTO_CAIXA = "AD_ENVFECHACAIXA";
	public static final String ENVIO_MOVIMENTO_CAIXA_DETALHE = "AD_ENVFECHACAIXADET";
	public static final MathContext MATH_CTX = new MathContext(64, RoundingMode.UP);

	private final CredentialsDTO credentialsDTO;
	private final List<DynamicVO> cabVOs = new ArrayList<DynamicVO>();
	private final List<FechaMovimentoDTO> fechamentos = new ArrayList<FechaMovimentoDTO>();

	public EnviarMovimentoService(CredentialsDTO credentialsDTO) {
		this.credentialsDTO = credentialsDTO;
	}

	public void adicionarNota(DynamicVO cabVO) {
		cabVOs.add(cabVO);
	}

	public void adicionarFechamento(FechaMovimentoDTO fechaMovimentoDTO) {
		fechamentos.add(fechaMovimentoDTO);
	}

	@Override
	public void run() {
		for (DynamicVO cabVO : cabVOs) {
			enviarMovimento(cabVO);
		}
		
		for (FechaMovimentoDTO fechaMovimentoDTO : fechamentos) {
			enviarFechamento(fechaMovimentoDTO);
		}
	}

	@SuppressWarnings("unchecked")
	private void enviarMovimento(DynamicVO cabVO) {
		final MovimentoVendaDTO movimentoVendaDTO = new MovimentoVendaDTO();
		try {
			movimentoVendaDTO.setCodEmpresa(cabVO.asBigDecimal("CODEMP"));
			movimentoVendaDTO.setNuCaixa(cabVO.asBigDecimal("NROCAIXA"));
			movimentoVendaDTO.setNuNota(cabVO.asBigDecimal("NUNOTA"));
			movimentoVendaDTO.setDhVenda(cabVO.asTimestamp("DTNEG"));
			movimentoVendaDTO.setNumeroNota(cabVO.asBigDecimal("NUNOTA").toPlainString());
			movimentoVendaDTO.setVlrDesconto(cabVO.asBigDecimalOrZero("VLRDESCTOTITEM"));
			movimentoVendaDTO.setVlrAcrescimo(cabVO.asBigDecimalOrZero("VLRDESTAQUE"));
			movimentoVendaDTO.setVlrTotal(cabVO.asBigDecimal("VLRNOTA"));
			movimentoVendaDTO.setCancelado(false);

			// Pega CPF/CNPJ da venda.
			final String cpfCnpj;
			BigDecimal codParc = cabVO.asBigDecimalOrZero("CODPARC");
			BigDecimal codParcPadrao = NativeSql.getBigDecimal("CODPARCNFCE", "TGFEMP", "CODEMP = ?",
					new Object[] { movimentoVendaDTO.getCodEmpresa() });
			if (codParc.compareTo(codParcPadrao) == 0) { // � o parceiro padr�o.
				cpfCnpj = cabVO.asString("CPFCNPJADQUIRENTE");
			} else {
				cpfCnpj = NativeSql.getString("CGC_CPF", "TGFPAR", "CODPARC = ?",
						new Object[] { cabVO.asBigDecimalOrZero("CODPARC") });
			}
			movimentoVendaDTO.setCpf(StringUtils.getOrEmpty(cpfCnpj).trim());

			Collection<DynamicVO> itemCollection = (Collection<DynamicVO>) cabVO
					.asCollection(DynamicEntityNames.ITEM_NOTA);
			for (DynamicVO itemVO : itemCollection) {
				DynamicVO prodVO = itemVO.asDymamicVO(DynamicEntityNames.PRODUTO);
				MovimentoVendaItemDTO movimentoVendaItemDTO = new MovimentoVendaItemDTO();
				movimentoVendaItemDTO.setCodigoProduto(prodVO.asBigDecimal("CODPROD").toPlainString());
				movimentoVendaItemDTO.setCodigoBarras(prodVO.asString("REFERENCIA"));
				movimentoVendaItemDTO.setDescrProduto(prodVO.asString("DESCRPROD"));
				movimentoVendaItemDTO.setQuantidade(itemVO.asBigDecimalOrZero("QTDNEG"));
				movimentoVendaItemDTO.setVlrUnitario(itemVO.asBigDecimalOrZero("VLRUNIT"));
				movimentoVendaItemDTO.setVlrItem(
						itemVO.asBigDecimal("VLRTOT").subtract(itemVO.asBigDecimalOrZero("VLRDESC"), MATH_CTX));
				movimentoVendaItemDTO.setVlrDesconto(itemVO.asBigDecimal("VLRDESC"));
				movimentoVendaItemDTO.setVlrAcrescimo(BigDecimal.ZERO);
				movimentoVendaDTO.getItens().add(movimentoVendaItemDTO);
			}

			Collection<DynamicVO> finCollection = (Collection<DynamicVO>) cabVO
					.asCollection(DynamicEntityNames.FINANCEIRO);
			for (DynamicVO finVO : finCollection) {
				DynamicVO tipTitVO = finVO.asDymamicVO(DynamicEntityNames.TIPO_TITULO);
				MovimentoVendaFinanceiroDTO movimentoVendaFinanceiroDTO = new MovimentoVendaFinanceiroDTO();
				movimentoVendaFinanceiroDTO
						.setTipoPagamento(TipoPagamento.buscarCodigoPorEspecie(tipTitVO.asString("ESPDOC")));
				movimentoVendaFinanceiroDTO.setVlrDesdobramento(finVO.asBigDecimal("VLRDESDOB"));
				movimentoVendaDTO.getFinanceiros().add(movimentoVendaFinanceiroDTO);
			}

			credentialsDTO.setNuCaixa(movimentoVendaDTO.getNuCaixa());
			WSCaller wsCaller = new WSCaller(credentialsDTO);
			StringRequestEntity body = new StringRequestEntity(JsonUtils.toJSON(movimentoVendaDTO), "application/json",
					"UTF-8");
			String resposta = wsCaller.executePOST(URLService.ENVIAR_MOVIMENTO, body);
			persistirEnvioMovimento(movimentoVendaDTO, 1, resposta);
		} catch (WSCallerException e) {
			if (e.getHttpCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR
					|| e.getHttpCode() == HttpStatus.SC_REQUEST_TIMEOUT) {
				persistirEnvioMovimento(movimentoVendaDTO, 0,
						String.format("HTTP%s: %s", String.valueOf(e.getHttpCode()), e.getMessage()));
			} else {
				persistirEnvioMovimento(movimentoVendaDTO, -1,
						String.format("HTTP%s: %s", String.valueOf(e.getHttpCode()), e.getMessage()));
			}
		} catch (Exception e) {
			persistirEnvioMovimento(movimentoVendaDTO, -1,
					String.format("%s: %s", e.getClass().toString(), e.getMessage()));
		}

	}

	private void enviarFechamento(FechaMovimentoDTO fechamento) {
		try {
			credentialsDTO.setNuCaixa(fechamento.getNuCaixa());
			WSCaller wsCaller = new WSCaller(credentialsDTO);
			StringRequestEntity body = new StringRequestEntity(JsonUtils.toJSON(fechamento), "application/json", "UTF-8");
			String resposta = wsCaller.executePOST(URLService.FECHAMENTO_DIARIO, body);
			persistirEnvioFechamento(fechamento, 1, resposta);
		} catch (WSCallerException e) {
			if (e.getHttpCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR
					|| e.getHttpCode() == HttpStatus.SC_REQUEST_TIMEOUT) {
				persistirEnvioFechamento(fechamento, 0, String.format("HTTP%s: %s", String.valueOf(e.getHttpCode()), e.getMessage()));
			} else {
				persistirEnvioFechamento(fechamento, -1, String.format("HTTP%s: %s", String.valueOf(e.getHttpCode()), e.getMessage()));
			}
		} catch (Exception e) {
			persistirEnvioFechamento(fechamento, -1, String.format("%s: %s", e.getClass().toString(), e.getMessage()));
		}
	}
	
	private void persistirEnvioMovimento(final MovimentoVendaDTO movVendaDTO, final int status, final String resposta) {
		SessionHandle hnd = null;
		try {
			hnd = JapeSession.open();
			hnd.setCanTimeout(false);

			hnd.execEnsuringTX(new JapeSession.TXBlock() {
				@Override
				public void doWithTx() throws Exception {
					EntityFacade dwf = null;
					JdbcWrapper jdbc = null;
					try {
						dwf = EntityFacadeFactory.getDWFFacade();
						jdbc = dwf.getJdbcWrapper();
						jdbc.openSession();
						jdbc.setQueryTimeout(0);

						boolean alreadyExists = false;
						DynamicVO movCaixaDetVO = null;
						try {
							movCaixaDetVO = (DynamicVO) dwf.findEntityByPrimaryKeyAsVO(ENVIO_MOVIMENTO_CAIXA_DETALHE,
									new Object[] { movVendaDTO.getCodEmpresa(), movVendaDTO.getNuCaixa(),
											movVendaDTO.getNuNota() });
							alreadyExists = true;
						} catch (Exception e) {
							movCaixaDetVO = (DynamicVO) dwf
									.getDefaultValueObjectInstance(ENVIO_MOVIMENTO_CAIXA_DETALHE);
							movCaixaDetVO.setProperty("CODEMP", movVendaDTO.getCodEmpresa());
							movCaixaDetVO.setProperty("NUCAIXA", movVendaDTO.getNuCaixa());
							movCaixaDetVO.setProperty("NUNOTA", movVendaDTO.getNuNota());
						}

						movCaixaDetVO.setProperty("TIPMOV", movVendaDTO.isCancelado() ? "C" : "V");
						movCaixaDetVO.setProperty("STATUS", status == 0 ? "P" : (status == -1 ? "E" : "S"));
						movCaixaDetVO.setProperty("EXTRAS", JsonUtils.toJSON(movVendaDTO).toCharArray());
						movCaixaDetVO.setProperty("RETORNO", StringUtils.getOrEmpty(resposta).toCharArray());
						movCaixaDetVO.setProperty("DHENVIO", new Timestamp(System.currentTimeMillis()));

						if (alreadyExists) {
							dwf.saveEntity(ENVIO_MOVIMENTO_CAIXA_DETALHE, (EntityVO) movCaixaDetVO);
						} else {
							dwf.createEntity(ENVIO_MOVIMENTO_CAIXA_DETALHE, (EntityVO) movCaixaDetVO);
						}

					} catch (Exception e) {
						StringBuilder msgBuilder = new StringBuilder(
								"Erro ao tentar registrar envio de Movimento abaixo\n");
						msgBuilder.append(String.format("C�d. Empresa: %s, N� Caixa: %s, N� Nota: %s\n",
								movVendaDTO.getCodEmpresa().toPlainString(), movVendaDTO.getNuCaixa().toPlainString(),
								movVendaDTO.getNuNota().toPlainString()));
						msgBuilder.append("Erro: " + e.getMessage());
						System.out.print(msgBuilder.toString());
					} finally {
						JdbcWrapper.closeSession(jdbc);
					}
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			JapeSession.close(hnd);
		}
	}
		
		private void persistirEnvioFechamento(final FechaMovimentoDTO fechamento, final int status, final String resposta) {
			SessionHandle hnd = null;
			try {
				hnd = JapeSession.open();
				hnd.setCanTimeout(false);

				hnd.execEnsuringTX(new JapeSession.TXBlock() {
					@Override
					public void doWithTx() throws Exception {
						EntityFacade dwf = null;
						JdbcWrapper jdbc = null;
						try {
							dwf = EntityFacadeFactory.getDWFFacade();
							jdbc = dwf.getJdbcWrapper();
							jdbc.openSession();
							jdbc.setQueryTimeout(0);

							boolean alreadyExists = false;
							DynamicVO fechaCaixaVO = null;
							try {
								fechaCaixaVO = (DynamicVO) dwf.findEntityByPrimaryKeyAsVO(ENVIO_MOVIMENTO_CAIXA, new Object[] { fechamento.getCodEmpresa(), fechamento.getNuCaixa()});
								alreadyExists = true;
							} catch (Exception e) {
								fechaCaixaVO = (DynamicVO) dwf.getDefaultValueObjectInstance(ENVIO_MOVIMENTO_CAIXA);
								fechaCaixaVO.setProperty("CODEMP", fechamento.getCodEmpresa());
								fechaCaixaVO.setProperty("NUCAIXA", fechamento.getNuCaixa());
							}

							fechaCaixaVO.setProperty("VLRVENDALIQUIDA", fechamento.getVlrVendaLiquida());
							fechaCaixaVO.setProperty("VLRVENDACANCELDA", fechamento.getVlrCancelamento());
							fechaCaixaVO.setProperty("QTDMOVVENDA", fechamento.getQtdVendaLiquida());
							fechaCaixaVO.setProperty("QTDMOVCANCELADA", fechamento.getQtdCancelamento());
							fechaCaixaVO.setProperty("DHCAIXA", fechamento.getDhCaixa());
							fechaCaixaVO.setProperty("STATUS", status == 0 ? "P" : (status == -1 ? "E" : "S"));
							if (alreadyExists) {
								dwf.saveEntity(ENVIO_MOVIMENTO_CAIXA, (EntityVO) fechaCaixaVO);
							} else {
								dwf.createEntity(ENVIO_MOVIMENTO_CAIXA, (EntityVO) fechaCaixaVO);
							}

						} catch (Exception e) {
							StringBuilder msgBuilder = new StringBuilder(
									"Erro ao tentar registrar envio de Fechamento abaixo\n");
							msgBuilder.append(String.format("C�d. Empresa: %s, N� Caixa: %s\n", fechamento.getCodEmpresa().toPlainString(), fechamento.getNuCaixa().toPlainString()));
							msgBuilder.append("Erro: " + e.getMessage());
							System.out.print(msgBuilder.toString());
						} finally {
							JdbcWrapper.closeSession(jdbc);
						}
					}
				});
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				JapeSession.close(hnd);
			}
	}
}
