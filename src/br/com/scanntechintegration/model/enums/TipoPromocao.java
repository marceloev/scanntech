package br.com.scanntechintegration.model.enums;

import lombok.Getter;

@Getter
public enum TipoPromocao {

	LLEVA_PAGA("Leva e Paga"), PRECIO_FIJO("Pre�o Fixo"), ADICIONAL_DESCUENTO("Um desconto em determinado produto"),
	ADICIONAL_REGALO("Um Brinde"), DESCUENTO_VARIABLE("Desconto Vari�vel"), DESCUENTO_FIJO("Desconto Fixo");

	private String descricao;

	private TipoPromocao(String descricao) {
		this.descricao = descricao;
	}
}
