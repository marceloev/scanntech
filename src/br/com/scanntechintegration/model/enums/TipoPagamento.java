package br.com.scanntechintegration.model.enums;

import lombok.Getter;

@Getter
public enum TipoPagamento {

	DINHEIRO(9),
	CARTAO_CREDITO(10),
	CHEQUE(11),
	VOUCHER(12),
	CARTAO_DEBITO(13);
	
	private int codigo;
	
	private TipoPagamento(int codigo) {
		this.codigo = codigo;
	}
	
	public static int buscarCodigoPorEspecie(String especie) {
		if ("CC".equals(especie)) {
			return CARTAO_CREDITO.getCodigo();
		} else if ("CD".equals(especie)) {
			return CARTAO_DEBITO.getCodigo();
		} else if ("CH".equals(especie)) {
			return CHEQUE.getCodigo();
		} else if ("CA".equals(especie)) {
			return VOUCHER.getCodigo();
		} else {
			return DINHEIRO.getCodigo();
		}
	}
}
