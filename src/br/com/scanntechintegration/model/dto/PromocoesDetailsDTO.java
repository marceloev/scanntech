package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import br.com.scanntechintegration.model.enums.TipoPromocao;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class PromocoesDetailsDTO {

	@SerializedName("articulos")
	private List<PromocoesProdutoDTO> produtos;
	
	@SerializedName("beneficios")
	private List<PromocoesProdutoDTO> brindes;
	
	@SerializedName("cantidad")
	private BigDecimal quantidade;
	
	@SerializedName("precio")
	private BigDecimal preco;
	
	@SerializedName("descuento")
	private BigDecimal desconto;
	
	@SerializedName("paga")
	private BigDecimal paga;
	
	public BigDecimal getVlrVenda(TipoPromocao tipoPromocao) {
		if (TipoPromocao.PRECIO_FIJO == tipoPromocao) {
			return this.getPreco();
		}
		
		return BigDecimal.ZERO;
	}

	public String isUsaDescQtd(TipoPromocao tipoPromocao) {
		//return (this.getQuantidade() == null || BigDecimal.ZERO.compareTo(this.getQuantidade()) == 0) ? "N" : "S";
		return "S";
	}
	
	public BigDecimal getVlrDesconto(TipoPromocao tipoPromocao) {
		return BigDecimal.ZERO;
	}
	
	public BigDecimal getPercDesconto(TipoPromocao tipoPromocao) {
		return BigDecimal.ZERO;
	}
}
