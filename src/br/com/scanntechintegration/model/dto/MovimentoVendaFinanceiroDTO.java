package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MovimentoVendaFinanceiroDTO {

	@SerializedName("codigoTipoPago")
	private int tipoPagamento;

	@SerializedName("codigoMoneda")
	private String codigoMoeda = "986";

	@SerializedName("importe")
	private BigDecimal vlrDesdobramento;

	@SerializedName("cotizacion")
	private BigDecimal cotacao = BigDecimal.ONE;
	
	@SerializedName("documentoCliente")
	private String cpf;

	@SerializedName("bin")
	private String bin;
	
	@SerializedName("ultimosDigitosTarjeta")
	private String digitoTarjeta;
	
	@SerializedName("numeroAutorizacion")
	private String numeroAutorizacao;
	
	@SerializedName("codigoTarjeta")
	private String codigoTarjeta;
}
