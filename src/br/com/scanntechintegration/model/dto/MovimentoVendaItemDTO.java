package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MovimentoVendaItemDTO {

	@SerializedName("codigoArticulo")
	private String codigoProduto;

	@SerializedName("codigoBarras")
	private String codigoBarras;
	
	@SerializedName("descripcionArticulo")
	private String descrProduto;
	
	@SerializedName("cantidad")
	private BigDecimal quantidade;

	@SerializedName("importeUnitario")
	private BigDecimal vlrUnitario;

	@SerializedName("importe")
	private BigDecimal vlrItem;

	@SerializedName("descuento")
	private BigDecimal vlrDesconto;
	
	@SerializedName("recargo")
	private BigDecimal vlrAcrescimo;
}
