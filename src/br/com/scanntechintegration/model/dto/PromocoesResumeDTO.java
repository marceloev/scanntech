package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class PromocoesResumeDTO {

	private BigDecimal codEmp;
	private BigDecimal idPromocao;
}
