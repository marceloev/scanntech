package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import br.com.sankhya.jape.sql.NativeSql;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class PromocoesProdutoDTO {

	private BigDecimal codEmp;
	private BigDecimal idItem;
	private BigDecimal idProd;
	
	@SerializedName("nombre")
	private String nome;
	
	@SerializedName("codigoBarras")
	private String ean;
	
	public void feedParams(NativeSql nativeSql) {
		nativeSql.setNamedParameter("CODEMP", this.getCodEmp());
		nativeSql.setNamedParameter("ID", this.getIdItem());
		nativeSql.setNamedParameter("IDPROD", this.getIdProd());
		nativeSql.setNamedParameter("NOME", this.getNome());
		nativeSql.setNamedParameter("CODIGOBARRAS", this.getEan());
	}
}
