package br.com.scanntechintegration.model.dto;

import lombok.Data;

@Data
public class EmpresaDTO {

	private Long id;
	private String name;
}
