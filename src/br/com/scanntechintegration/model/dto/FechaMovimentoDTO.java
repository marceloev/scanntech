package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class FechaMovimentoDTO {
	
	private BigDecimal codEmpresa;
	
	private BigDecimal nuCaixa;
	
	@SerializedName("fechaVentas")
	private Timestamp dhCaixa;
	
	@SerializedName("montoVentaLiquida")
	private BigDecimal vlrVendaLiquida;
	
	@SerializedName("cantidadMovimientos")
	private BigDecimal qtdVendaLiquida;
	
	@SerializedName("montoCancelaciones")
	private BigDecimal vlrCancelamento;
	
	@SerializedName("cantidadCancelaciones")
	private BigDecimal qtdCancelamento;

}
