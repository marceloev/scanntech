package br.com.scanntechintegration.model.dto;

public class BusinessException extends Exception {
	
	private static final long serialVersionUID = 5629782080410043240L;
	
	public BusinessException(String string) {
		super(string);
	}

}
