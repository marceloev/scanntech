package br.com.scanntechintegration.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class PromocoesSummaryDTO {

	private Long total;
	private List<PromocoesResultDTO> results;
}
