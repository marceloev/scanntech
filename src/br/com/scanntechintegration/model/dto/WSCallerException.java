package br.com.scanntechintegration.model.dto;

import lombok.Getter;

@Getter
public class WSCallerException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private final int httpCode;

	public WSCallerException(String message) {
		this(message, 0);
	}
	
	public WSCallerException(String message, int httpCode) {
		super(message);
		this.httpCode = httpCode;
	}

}
