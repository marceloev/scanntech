package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MovimentoVendaDTO {
	
	private BigDecimal codEmpresa;
	private BigDecimal nuCaixa;
	private BigDecimal nuNota;
	
	@SerializedName("fecha")
	private Timestamp dhVenda;
	
	@SerializedName("numero")
	private String numeroNota;
	
	@SerializedName("descuentoTotal")
	private BigDecimal vlrDesconto;
	
	@SerializedName("recargoTotal")
	private BigDecimal vlrAcrescimo;
	
	@SerializedName("codigoMoneda")
	private String moeda = "986";
	
	@SerializedName("cotizacion")
	private BigDecimal cotacao = BigDecimal.ONE;
	
	@SerializedName("total")
	private BigDecimal vlrTotal;
	
	@SerializedName("cancelacion")
	private boolean cancelado;
	
	@SerializedName("documentoCliente")
	private String cpf;
	
	@SerializedName("codigoCanalVenta")
	private int tipoVenda = 1;
	
	@SerializedName("detalles")
	private List<MovimentoVendaItemDTO> itens = new ArrayList<MovimentoVendaItemDTO>();
	
	@SerializedName("pagos")
	private List<MovimentoVendaFinanceiroDTO> financeiros = new ArrayList<MovimentoVendaFinanceiroDTO>();
	
	

}
