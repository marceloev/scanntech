package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DescontoQuantidadeDTO {

	private BigDecimal nuPromocao;
	private BigDecimal quantidade;
	private BigDecimal percDesc;
	private BigDecimal descFixo;
	private String tipDesc;
}
