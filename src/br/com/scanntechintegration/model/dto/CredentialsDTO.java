package br.com.scanntechintegration.model.dto;

import java.math.BigDecimal;

import br.com.sankhya.extensions.actionbutton.Registro;
import lombok.Data;

@Data
public class CredentialsDTO {
	
	private BigDecimal codEmpresa;
	private String username;
	private String password;
	private String idLoja;
	private String idLocal;
	private BigDecimal codTab;
	private Boolean usaDescProgressivo;
	private BigDecimal nuCaixa;
	
	public CredentialsDTO(Registro registro) {
		this.codEmpresa = (BigDecimal) registro.getCampo("CODEMP");
		this.username = (String) registro.getCampo("USUARIO");
		this.password = (String) registro.getCampo("SENHA");
		this.idLoja = (String) registro.getCampo("LOJA");
		this.idLocal = (String) registro.getCampo("LOCAL");
		this.codTab = (BigDecimal) registro.getCampo("CODTAB");
		this.usaDescProgressivo = "S".equals((String) registro.getCampo("APLICADESCPROGRESSIVO"));
	}
	
	public static final CredentialsDTO buildFromRegistro(Registro registro) {
		return new CredentialsDTO(registro);
	}
}
