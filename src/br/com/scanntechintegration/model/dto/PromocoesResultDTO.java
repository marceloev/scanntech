package br.com.scanntechintegration.model.dto;

import br.com.sankhya.extensions.actionbutton.Registro;
import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.jape.util.FinderWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.scanntechintegration.model.enums.TipoPromocao;
import br.com.scanntechintegration.utils.BigDecimalUtils;
import br.com.scanntechintegration.utils.PromocaoEntityNames;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@ToString
@EqualsAndHashCode
public class PromocoesResultDTO {

    private static final MathContext MATH_CTX = new MathContext(32, RoundingMode.DOWN);

    private BigDecimal codEmp;
    private BigDecimal idItem;

    @SerializedName("id")
    private BigDecimal idScanntech;

    @SerializedName("titulo")
    private String titulo;

    @SerializedName("descripcion")
    private String description;

    private BigDecimal status;
    private BigDecimal statusIntegracao;

    @SerializedName("tipo")
    private TipoPromocao tipoPromocao;

    @SerializedName("vigenciaDesde")
    private Timestamp dhInicio;

    @SerializedName("vigenciaHasta")
    private Timestamp dhFim;

    @SerializedName("limitePromocionesPorTicket")
    private BigDecimal limiteQtd;
    
    private boolean usaDescProgressivo;

    private BigDecimal codTab;
    
    @SerializedName("detalles")
    private PromocoesDetailsDTO detalhes;

    @SuppressWarnings("unchecked")
    public static PromocoesResultDTO convertFromRegistro(BigDecimal codTab, Registro registro) throws Exception {
        EntityFacade dwf = EntityFacadeFactory.getDWFFacade();
        PromocoesResultDTO promocao = new PromocoesResultDTO();

        promocao.setCodEmp((BigDecimal) registro.getCampo("CODEMP"));
        promocao.setCodTab((BigDecimal) registro.getCampo("CODTAB"));
        promocao.setUsaDescProgressivo("S".equals(((String) registro.getCampo("APLICADESCPROGRESSIVO"))));
        promocao.setIdItem((BigDecimal) registro.getCampo("ID"));
        promocao.setIdScanntech((BigDecimal) registro.getCampo("IDSCANNTECH"));
        promocao.setTitulo((String) registro.getCampo("TITULO"));
        promocao.setDescription((String) registro.getCampo("DESCRICAO"));
        promocao.setStatus((BigDecimal) registro.getCampo("STATUS"));
        promocao.setStatusIntegracao(registro.getCampo("STATUSINT") == null ? BigDecimal.ZERO : BigDecimal.valueOf(Double.valueOf((String) registro.getCampo("STATUSINT"))));
        promocao.setTipoPromocao(TipoPromocao.values()[Integer.valueOf((String) registro.getCampo("TIPO"))]);
        promocao.setDhInicio((Timestamp) registro.getCampo("DHINICIO"));
        promocao.setDhFim((Timestamp) registro.getCampo("DHFIM"));
        promocao.setLimiteQtd((BigDecimal) registro.getCampo("LIMITE"));
        promocao.setCodTab(codTab);

        PromocoesDetailsDTO detalhes = new PromocoesDetailsDTO();
        detalhes.setQuantidade((BigDecimal) registro.getCampo("QTD"));
        detalhes.setPreco((BigDecimal) registro.getCampo("PRECO"));
        detalhes.setDesconto((BigDecimal) registro.getCampo("DESCONTO"));
        detalhes.setPaga((BigDecimal) registro.getCampo("PAGA"));

        FinderWrapper finderProd = new FinderWrapper(PromocaoEntityNames.PROD_INTEGRACAO_PROMO, "this.CODEMP = ? AND this.ID = ?", new Object[]{promocao.getCodEmp(), promocao.getIdItem()});
        Collection<DynamicVO> prodVOList = dwf.findByDynamicFinderAsVO(finderProd);

        ArrayList<PromocoesProdutoDTO> promocoesProdDTO = new ArrayList<PromocoesProdutoDTO>();
        for (DynamicVO prodVO : prodVOList) {
            PromocoesProdutoDTO promoProdDTO = new PromocoesProdutoDTO();
            promoProdDTO.setCodEmp(prodVO.asBigDecimal("CODEMP"));
            promoProdDTO.setIdItem(prodVO.asBigDecimal("ID"));
            promoProdDTO.setIdProd(prodVO.asBigDecimal("IDPROD"));
            promoProdDTO.setNome(prodVO.asString("NOME"));
            promoProdDTO.setEan(prodVO.asString("CODIGOBARRAS"));
            promocoesProdDTO.add(promoProdDTO);
        }
        detalhes.setProdutos(promocoesProdDTO);


        FinderWrapper finderBrinde = new FinderWrapper(PromocaoEntityNames.BRINDE_INTEGRACAO_PROMO, "this.CODEMP = ? AND this.ID = ?", new Object[]{promocao.getCodEmp(), promocao.getIdItem()});
        Collection<DynamicVO> brindeVOList = dwf.findByDynamicFinderAsVO(finderBrinde);

        ArrayList<PromocoesProdutoDTO> promocoesBrindesDTO = new ArrayList<PromocoesProdutoDTO>();
        for (DynamicVO brindeVO : brindeVOList) {
            PromocoesProdutoDTO promoBrindeDTO = new PromocoesProdutoDTO();
            promoBrindeDTO.setCodEmp(brindeVO.asBigDecimal("CODEMP"));
            promoBrindeDTO.setIdItem(brindeVO.asBigDecimal("ID"));
            promoBrindeDTO.setIdProd(brindeVO.asBigDecimal("IDPROD"));
            promoBrindeDTO.setNome(brindeVO.asString("NOME"));
            promoBrindeDTO.setEan(brindeVO.asString("CODIGOBARRAS"));
            promocoesBrindesDTO.add(promoBrindeDTO);
        }
        detalhes.setBrindes(promocoesBrindesDTO);

        promocao.setDetalhes(detalhes);

        return promocao;
    }

    public String buildCriterio() {
        StringBuilder criterio = new StringBuilder();

        switch (getTipoPromocao()) {
            case LLEVA_PAGA:
                criterio.append(String.format("Levando %s unidades de qualquer um dos seguintes itens, paga %s.\n", this.getDetalhes().getQuantidade().toPlainString(), this.getDetalhes().getPaga().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                break;
            case PRECIO_FIJO:
                criterio.append(String.format("Levando %s unidade do seguinte item\n", this.getDetalhes().getQuantidade().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                criterio.append(String.format("Paga um total de R$%s", this.getDetalhes().getPreco().toPlainString()));
                break;
            case ADICIONAL_DESCUENTO:
                criterio.append(String.format("Levando %s unidades de qualquer um dos seguintes itens\n", this.getDetalhes().getQuantidade().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                criterio.append(String.format("Receba um desconto de %s%% na compra de qualquer um destes itens\n", this.getDetalhes().getDesconto().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getBrindes()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                break;
            case ADICIONAL_REGALO:
                criterio.append(String.format("Levando %s unidades de qualquer um dos seguintes itens\n", this.getDetalhes().getQuantidade().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                criterio.append(String.format("Receba de presente qualquer um dos seguintes itens\n"));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getBrindes()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                break;
            case DESCUENTO_FIJO:
                criterio.append(String.format("Levando %s unidades de qualquer um dos seguintes itens\n", this.getDetalhes().getQuantidade().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                criterio.append(String.format("Deduz um total de R$%s", this.getDetalhes().getDesconto().toPlainString()));
                break;
            case DESCUENTO_VARIABLE:
                criterio.append(String.format("Levando %s unidades de qualquer um dos seguintes itens\n", this.getDetalhes().getQuantidade().toPlainString()));
                for (PromocoesProdutoDTO prodDTO : this.getDetalhes().getProdutos()) {
                    criterio.append(String.format("\t* %s - %s\n", prodDTO.getEan(), prodDTO.getNome()));
                }
                criterio.append(String.format("Desconto no total do pre�o %s%%\n", this.getDetalhes().getDesconto().toPlainString()));
                break;
            default:
                return "CRITERIO N�O PROMOGRADO!";
        }

        return criterio.toString();
    }

    public void feedParams(NativeSql nativeSql) {
        nativeSql.setNamedParameter("CODEMP", this.getCodEmp());
        nativeSql.setNamedParameter("ID", this.getIdItem());
        nativeSql.setNamedParameter("IDSCANNTECH", this.getIdScanntech());
        nativeSql.setNamedParameter("TITULO", this.getTitulo());
        nativeSql.setNamedParameter("DESCRICAO", this.getDescription());
        nativeSql.setNamedParameter("TIPO", String.valueOf(this.getTipoPromocao() == null ? "?" : this.getTipoPromocao().ordinal()));
        nativeSql.setNamedParameter("STATUSINT", null);
        nativeSql.setNamedParameter("STATUS", this.getStatus());
        nativeSql.setNamedParameter("DHINICIO", this.getDhInicio());
        nativeSql.setNamedParameter("DHFIM", this.getDhFim());
        nativeSql.setNamedParameter("PRECO", this.getDetalhes().getPreco());
        nativeSql.setNamedParameter("DESCONTO", this.getDetalhes().getDesconto());
        nativeSql.setNamedParameter("QTD", this.getDetalhes().getQuantidade());
        nativeSql.setNamedParameter("PAGA", this.getDetalhes().getPaga());
        nativeSql.setNamedParameter("LIMITE", this.getLimiteQtd());
        nativeSql.setNamedParameter("CRITERIO", this.buildCriterio());
    }

    public boolean isNotIntegrated() throws Exception {
        return BigDecimal.ZERO.compareTo(NativeSql.getBigDecimal("COUNT(1)", PromocaoEntityNames.ITEM_INTEGRACAO_PROMO, "CODEMP = ? AND IDSCANNTECH = ?", new Object[]{this.getCodEmp(), this.getIdScanntech()})) == 0;
    }

    public boolean isNotProcessed() {
        return this.getStatusIntegracao() == null || BigDecimal.ONE.compareTo(this.getStatusIntegracao()) != 0;
    }

    public List<DescontoQuantidadeDTO> getDescontosQuantidade(BigDecimal nuPromocao) throws Exception {
        List<DescontoQuantidadeDTO> descQtdList = new ArrayList<DescontoQuantidadeDTO>();
        Integer limiteQtd = (BigDecimalUtils.isNullOrZero(this.getLimiteQtd()) ? 500 : (this.getLimiteQtd().intValue() * this.getDetalhes().getQuantidade().intValue()));

        switch (this.tipoPromocao) {
			case DESCUENTO_VARIABLE:
				BigDecimal precoVendaDV = NativeSql.getBigDecimal("sankhya.SNK_PRECO(?, ?)", "DUAL", "1 = 1", 
						new Object[] { this.getCodTab(), this.getDetalhes().getProdutos().get(0).getIdProd() });
				
				for (int i = 0; i <= 999; i ++) {
					if (i < this.getDetalhes().getQuantidade().intValue()) {
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(i)).tipDesc("P").build());
					} if (i % this.getDetalhes().getQuantidade().intValue() == 0 && (BigDecimalUtils.isNullOrZero(this.getLimiteQtd()) || i <= (this.getDetalhes().getQuantidade().intValue() * this.getLimiteQtd().intValue()))) {
						Double desconto = this.getDetalhes().getDesconto().doubleValue();
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.valueOf(desconto)).quantidade(BigDecimal.valueOf(i)).tipDesc("P").build());
					} else {
						int maxQtdDesc = -1;
						if (!BigDecimalUtils.isNullOrZero(this.getLimiteQtd())) {
							maxQtdDesc = this.getDetalhes().getQuantidade().intValue() * this.getLimiteQtd().intValue();	
						}
						
						final int oldQtdDesc;
						if (maxQtdDesc > 0 && i > maxQtdDesc) {
							oldQtdDesc = BigDecimal.valueOf(maxQtdDesc).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
						} else {
							oldQtdDesc = BigDecimal.valueOf(i).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
						}
						
						final BigDecimal desconto = precoVendaDV.multiply(BigDecimal.valueOf(oldQtdDesc), MATH_CTX);
						
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(i)).descFixo(desconto).tipDesc("V").build());
					}
					
					
				}
				
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).tipDesc("P").percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(1000)).build());
			break;
			case DESCUENTO_FIJO:
				for (int i = 1; i <= limiteQtd; i ++) {
					BigDecimal desconto = BigDecimal.valueOf(i).divide(this.getDetalhes().getQuantidade(), MATH_CTX).setScale(0, RoundingMode.DOWN);
					desconto = desconto.multiply(this.getDetalhes().getDesconto(), MATH_CTX);
					
					descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).descFixo(desconto).quantidade(BigDecimal.valueOf(i)).tipDesc("V").build());
				}
				
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(++limiteQtd)).tipDesc("P").build());
				break;
			case LLEVA_PAGA:
				BigDecimal precoVendaLP = NativeSql.getBigDecimal("sankhya.SNK_PRECO(?, ?)", "DUAL", "1 = 1", new Object[] { this.getCodTab(), this.getDetalhes().getProdutos().get(0).getIdProd() });
				
				for (int i = 1; i <= 999; i++) {
					int maxQtdDesc = -1;
					if (!BigDecimalUtils.isNullOrZero(this.getLimiteQtd())) {
						maxQtdDesc = this.getDetalhes().getQuantidade().intValue() * this.getLimiteQtd().intValue();
					}

					final int oldQtdDesc;
					if (maxQtdDesc > 0 && i > maxQtdDesc) {
						oldQtdDesc = BigDecimal.valueOf(maxQtdDesc).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
					} else {
						oldQtdDesc = BigDecimal.valueOf(i).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
					}
					
					BigDecimal vlrDesconto = precoVendaLP.multiply(BigDecimal.valueOf(oldQtdDesc), MATH_CTX);
					
					descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(i)).descFixo(vlrDesconto).tipDesc("V").build());
				}
				
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).tipDesc("P").percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(1000)).build());
				break;
			case ADICIONAL_DESCUENTO:
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(this.getDetalhes().getDesconto()).quantidade(this.getDetalhes().getQuantidade()).tipDesc("P").build());
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).tipDesc("P").percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(limiteQtd + 1)).build());
				break;
			case PRECIO_FIJO:
				BigDecimal precoVendaPF = NativeSql.getBigDecimal("sankhya.SNK_PRECO(?, ?)", "DUAL", "1 = 1", 
						new Object[] { this.getCodTab(), this.getDetalhes().getProdutos().get(0).getIdProd() });
				
				for (int i = 1; i <= 999; i ++) {
					if (i < this.getDetalhes().getQuantidade().intValue()) {
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(i)).tipDesc("V").build());
					} else if (i % this.getDetalhes().getQuantidade().intValue() == 0 && (BigDecimalUtils.isNullOrZero(this.getLimiteQtd()) || i <= (this.getDetalhes().getQuantidade().intValue() * this.getLimiteQtd().intValue()))) {
						Double desconto = (this.getDetalhes().getVlrVenda(TipoPromocao.PRECIO_FIJO).doubleValue() * (i / this.getDetalhes().getQuantidade().intValue())) / i;
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(BigDecimal.valueOf(desconto)).quantidade(BigDecimal.valueOf(i)).tipDesc("V").build());
					} else {
						int maxQtdDesc = -1;
						if (!BigDecimalUtils.isNullOrZero(this.getLimiteQtd())) {
							maxQtdDesc = this.getDetalhes().getQuantidade().intValue() * this.getLimiteQtd().intValue();	
						}
						
						final int oldQtdDesc;
						if (maxQtdDesc > 0 && i > maxQtdDesc) {
							oldQtdDesc = BigDecimal.valueOf(maxQtdDesc).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
						} else {
							oldQtdDesc = BigDecimal.valueOf(i).divide(this.getDetalhes().getQuantidade(), MATH_CTX).intValue();
						}
						 
						BigDecimal oldVlrDesconto = this.getDetalhes().getVlrVenda(tipoPromocao).multiply(BigDecimal.valueOf(oldQtdDesc), MATH_CTX);

						BigDecimal vlrDesconto = precoVendaPF
									.multiply(BigDecimal.valueOf((i - (oldQtdDesc * this.getDetalhes().getQuantidade().intValue()))), MATH_CTX)
									.add(oldVlrDesconto, MATH_CTX)
									.divide(BigDecimal.valueOf(i), MATH_CTX);
						
						descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).percDesc(vlrDesconto).quantidade(BigDecimal.valueOf(i)).tipDesc("V").build());
					}
					
				}
				
				descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).tipDesc("P").percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(1000)).build());
				break;
		default:
			descQtdList.add(DescontoQuantidadeDTO.builder().nuPromocao(nuPromocao).tipDesc("P").percDesc(BigDecimal.ZERO).quantidade(BigDecimal.valueOf(999)).build());
			break;
		}

        return descQtdList;
    }
}