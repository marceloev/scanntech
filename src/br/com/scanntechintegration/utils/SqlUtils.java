package br.com.scanntechintegration.utils;

import java.sql.ResultSet;

public class SqlUtils {

	public static void closeResultSet(ResultSet rs) {
		try {
			rs.close();
		} catch (Exception ignored) {}
	}
}
