package br.com.scanntechintegration.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StringUtils {

	public static final String convertThrowableToString(Throwable e) {
		if (e == null) {
			return "";
		} else {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return sw.toString();
		}
	}
	
	public static final String getOrEmpty(String arg) {
		return isNullOrEmpty(arg) ? "" : arg;
	}
	
	public static final boolean isNullOrEmpty(String arg) {
		return arg == null || arg.trim().length() == 0 || "null".equals(arg);
	}
}
