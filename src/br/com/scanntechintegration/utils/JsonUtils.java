package br.com.scanntechintegration.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import br.com.scanntechintegration.config.PromocoesDetalheDeserializer;
import br.com.scanntechintegration.model.dto.PromocoesDetailsDTO;

public class JsonUtils {
	private static final Gson GSON = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
			.registerTypeAdapter(PromocoesDetailsDTO.class, new PromocoesDetalheDeserializer())
			.serializeNulls()
			.create();
	
	public static final String toJSON(Object obj) {
		return GSON.toJson(obj);
	}
	
	public static final <T> T fromJSON(String json, Class<T> clazz) {
		return GSON.fromJson(json, clazz);
	}
	
	public static final <T> T fromJSON(InputStream inptStream, Class<T> clazz) throws UnsupportedEncodingException {
		Reader reader = new InputStreamReader(inptStream, "UTF-8");
		return GSON.fromJson(reader, clazz);
	}
	
	public static final <T> T fromJSON(JsonElement jElem, Class<T> clazz) {
		return GSON.fromJson(jElem, clazz);
	}
	
	public static final String toString(InputStream inptStream) throws UnsupportedEncodingException {
		Reader reader = new InputStreamReader(inptStream, "UTF-8");
		return GSON.fromJson(reader, Map.class).toString();
	}
	
	public static final <T> List<T> parseElementAsList(JsonElement jElem, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		if (jElem.isJsonArray()) {
			for (JsonElement jElemList : jElem.getAsJsonArray()) {
				list.add(GSON.fromJson(jElemList, clazz));
			}
		}
		return list;
	}
	
	public static String getSafetyString(JsonElement jElem, String member) {
		return isMemberValid(jElem, member) ? jElem.getAsJsonObject().get(member).getAsString() : null; 
	}
	
	public static BigDecimal getSafetyBigDecimal(JsonElement jElem, String member) {
		 return isMemberValid(jElem, member) ? jElem.getAsJsonObject().get(member).getAsBigDecimal() : null; 
	}
	
	public static boolean hasMember(JsonElement jElem, String... members) {
		boolean has = false;
		
		for (int i = 0; i < members.length; i++) {
			jElem = JsonUtils.isMemberValid(jElem, members[i]) ? jElem.getAsJsonObject().get(members[i]) : null;
			if (jElem == null) {
				break;
			}
			
			if (i == members.length - 1) {
				has = true;
			}			
		}
		
		return has;
	}
	
	public static JsonElement getMember(JsonElement jElem, String... members) {
		for (int i = 0; i < members.length; i++) {
			jElem = JsonUtils.isMemberValid(jElem, members[i]) ? jElem.getAsJsonObject().get(members[i]) : null;
			if (jElem == null) {
				break;
			}	
		}
		
		return jElem;
	}
	
	public static boolean isMemberValid(JsonElement jElem, String member) {
		return jElem.isJsonObject() && jElem.getAsJsonObject().has(member) && jElem.getAsJsonObject().get(member) != null && !jElem.getAsJsonObject().get(member).isJsonNull();
	}
}
