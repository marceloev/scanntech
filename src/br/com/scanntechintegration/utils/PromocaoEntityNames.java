package br.com.scanntechintegration.utils;

public class PromocaoEntityNames {

	public static final String CAB_INTEGRACAO_PROMO = "AD_CABINTPROMO";
	public static final String ITEM_INTEGRACAO_PROMO = "AD_ITEINTPROMO";
	public static final String PROD_INTEGRACAO_PROMO = "AD_ITEPRODINTPROMO";
	public static final String BRINDE_INTEGRACAO_PROMO = "AD_ITEBRINDEINTPROMO";
}
