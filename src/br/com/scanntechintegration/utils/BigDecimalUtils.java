package br.com.scanntechintegration.utils;

import java.math.BigDecimal;

public class BigDecimalUtils {

	public static boolean isNullOrZero(BigDecimal arg) {
		return arg == null || BigDecimal.ZERO.compareTo(arg) == 0;
	}
	
	public static BigDecimal valueOrZero(BigDecimal arg) {
		return isNullOrZero(arg) ? BigDecimal.ZERO : arg;
	}
}
