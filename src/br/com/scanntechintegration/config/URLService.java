package br.com.scanntechintegration.config;

import br.com.scanntechintegration.model.dto.CredentialsDTO;

public enum URLService {

	RECUPERA_PROMOCOES("/pmkt-rest-api/minoristas/{idEmpresa}/locales/{idLocal}/promocionesConLimitePorTicket"),
	ENVIAR_MOVIMENTO("/api-minoristas/api/v2/minoristas/{idEmpresa}/locales/{idLocal}/cajas/{idCaja}/movimientos"),
	FECHAMENTO_DIARIO("/api-minoristas/api/v2/minoristas/{idEmpresa}/locales/{idLocal}/cajas/{idCaja}/cierresDiarios");
	
	private String url;
	
	private URLService(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
	
	public final String getFinalURL(String host, CredentialsDTO credentialsDTO) {
		StringBuilder finalURL = new StringBuilder(host.concat(this.url));
		replaceAll(finalURL, "{idEmpresa}", credentialsDTO.getIdLoja());
		replaceAll(finalURL, "{idLocal}", credentialsDTO.getIdLocal());
		
		if (credentialsDTO.getNuCaixa() != null) {
			replaceAll(finalURL, "{idCaja}", credentialsDTO.getNuCaixa().toString());	
		}
		
		return finalURL.toString();
	}
	
	private final void replaceAll(StringBuilder sb, String toReplace, String replacement) {
		int index = -1;
		while ((index = sb.lastIndexOf(toReplace)) != -1) {
			sb.replace(index, index + toReplace.length(), replacement);
		}
	}
}
