package br.com.scanntechintegration.config;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import br.com.scanntechintegration.model.dto.PromocoesDetailsDTO;
import br.com.scanntechintegration.model.dto.PromocoesProdutoDTO;
import br.com.scanntechintegration.utils.JsonUtils;

public class PromocoesDetalheDeserializer implements JsonDeserializer<PromocoesDetailsDTO>{

	@Override
	public PromocoesDetailsDTO deserialize(JsonElement jElem, Type type, JsonDeserializationContext ctx) throws JsonParseException {
		PromocoesDetailsDTO promocoesDetailsDTO = new PromocoesDetailsDTO();
		
		if (jElem.isJsonObject()) {
			JsonObject jObj = jElem.getAsJsonObject();
			promocoesDetailsDTO.setPreco(JsonUtils.getSafetyBigDecimal(jObj, "precio"));
			promocoesDetailsDTO.setDesconto(JsonUtils.getSafetyBigDecimal(jObj, "descuento"));
			promocoesDetailsDTO.setPaga(JsonUtils.getSafetyBigDecimal(jObj, "paga"));
			
			JsonElement jElemItems = JsonUtils.getMember(jElem, "condiciones", "items");
			if (jElemItems != null && jElemItems.isJsonArray() && jElemItems.getAsJsonArray().size() > 0) {
				promocoesDetailsDTO.setQuantidade(JsonUtils.getSafetyBigDecimal(jElemItems.getAsJsonArray().get(0), "cantidad"));
				JsonElement jElemArticulos = JsonUtils.getMember(jElemItems.getAsJsonArray().get(0), "articulos");
				promocoesDetailsDTO.setProdutos(JsonUtils.parseElementAsList(jElemArticulos, PromocoesProdutoDTO.class));
			}
			
			JsonElement jElemBeneficios = JsonUtils.getMember(jElem, "beneficios", "items");
			if (jElemBeneficios != null && jElemBeneficios.isJsonArray() && jElemBeneficios.getAsJsonArray().size() > 0) {
				JsonElement jElemArticulos = JsonUtils.getMember(jElemBeneficios.getAsJsonArray().get(0), "articulos");
				promocoesDetailsDTO.setBrindes(JsonUtils.parseElementAsList(jElemArticulos, PromocoesProdutoDTO.class));
			} else {
				promocoesDetailsDTO.setBrindes(new ArrayList<PromocoesProdutoDTO>());
			}
		}		
		
		return promocoesDetailsDTO;
	}
	
	
	

}
