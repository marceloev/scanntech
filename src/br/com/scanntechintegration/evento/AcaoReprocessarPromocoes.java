package br.com.scanntechintegration.evento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.extensions.actionbutton.Registro;
import br.com.scanntechintegration.model.dto.PromocoesResultDTO;
import br.com.scanntechintegration.service.ProcessarDescontoService;

public class AcaoReprocessarPromocoes implements AcaoRotinaJava {

	@Override
	public void doAction(ContextoAcao ctx) throws Exception {
		Registro[] registros = ctx.getLinhas();
		if (registros == null || registros.length < 1) {
			ctx.mostraErro("� necess�rio selecionar no m�nimo 1 promo��o para reprocessar!");
			return;
		}
		
		List<PromocoesResultDTO> promocoes = new ArrayList<PromocoesResultDTO>();
		for (Registro registro : registros) {
			BigDecimal codTab = (BigDecimal) ctx.getLinhaPai().getCampo("CODTAB");
			PromocoesResultDTO promocao = PromocoesResultDTO.convertFromRegistro(codTab, registro);
			if (BigDecimal.ONE.compareTo(promocao.getStatusIntegracao()) != 0) {
				promocoes.add(promocao);	
			}
		}
		
		if (promocoes.size() == 0) {
			ctx.mostraErro("Nenhuma das promomo��es selecionadas estava Pendente ou Com Erro!");
		} else {
			new ProcessarDescontoService().processarPromocoes(promocoes);
			ctx.setMensagemRetorno("Promo��es reprocessadas com sucesso!");
		}
	}

}
