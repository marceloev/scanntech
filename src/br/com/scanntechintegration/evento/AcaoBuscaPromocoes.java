package br.com.scanntechintegration.evento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.extensions.actionbutton.Registro;
import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.dao.JdbcWrapper;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.scanntechintegration.config.URLService;
import br.com.scanntechintegration.model.dto.CredentialsDTO;
import br.com.scanntechintegration.model.dto.PromocoesProdutoDTO;
import br.com.scanntechintegration.model.dto.PromocoesResultDTO;
import br.com.scanntechintegration.model.dto.PromocoesSummaryDTO;
import br.com.scanntechintegration.service.ProcessarDescontoService;
import br.com.scanntechintegration.service.WSCaller;
import br.com.scanntechintegration.sql.SQLLoader;
import br.com.scanntechintegration.utils.JsonUtils;
import br.com.scanntechintegration.utils.PromocaoEntityNames;

public class AcaoBuscaPromocoes implements AcaoRotinaJava {

	@Override
	public void doAction(ContextoAcao ctx) throws Exception {
		Registro[] registros = ctx.getLinhas();
		if (registros == null || registros.length != 1) {
			ctx.mostraErro("� necess�rio selecionar uma linha!");
			return;
		}
		
		EntityFacade dwf = null;
		JdbcWrapper jdbc = null;
		NativeSql queInsereItemPromo = null;
		NativeSql queInsereProdPromo = null;
		NativeSql queInsereBrindePromo = null;
		try {
			dwf = EntityFacadeFactory.getDWFFacade();
			jdbc = dwf.getJdbcWrapper();
			
			CredentialsDTO credentialsDTO = CredentialsDTO.buildFromRegistro(registros[0]);
			WSCaller wsCaller = new WSCaller(credentialsDTO);
			
			PromocoesSummaryDTO promocoesSummaryDTO = JsonUtils.fromJSON(wsCaller.executeInptGET(URLService.RECUPERA_PROMOCOES), PromocoesSummaryDTO.class);
			if (promocoesSummaryDTO.getTotal() <= 0) {
				ctx.setMensagemRetorno("N�o foram encontradas promo��es!");
				return;
			}
			
			queInsereItemPromo = new NativeSql(jdbc, SQLLoader.class, "queInsertItemIntegracao.sql");
			queInsereItemPromo.setReuseStatements(true);
			queInsereItemPromo.setBatchUpdateSize(500);
			queInsereItemPromo.setFillNamedParametersWithNull(true);
			
			queInsereProdPromo = new NativeSql(jdbc, SQLLoader.class, "queInsertProdutoIntegracao.sql");
			queInsereProdPromo.setReuseStatements(true);
			queInsereProdPromo.setBatchUpdateSize(2000);
			queInsereProdPromo.setFillNamedParametersWithNull(true);
			
			queInsereBrindePromo = new NativeSql(jdbc, SQLLoader.class, "queInsertBrindeIntegracao.sql");
			queInsereBrindePromo.setReuseStatements(true);
			queInsereBrindePromo.setBatchUpdateSize(2000);
			queInsereBrindePromo.setFillNamedParametersWithNull(true);
			
			BigDecimal idItemPromo = NativeSql.getBigDecimal("ISNULL(MAX(ID), 0)", PromocaoEntityNames.ITEM_INTEGRACAO_PROMO, "1 = 1");
			List<PromocoesResultDTO> integratedPromocoes = new ArrayList<PromocoesResultDTO>();
			for (PromocoesResultDTO promocaoResultDTO : promocoesSummaryDTO.getResults()) {
				
				promocaoResultDTO.setCodEmp(credentialsDTO.getCodEmpresa());
				promocaoResultDTO.setCodTab(credentialsDTO.getCodTab());
				promocaoResultDTO.setUsaDescProgressivo(credentialsDTO.getUsaDescProgressivo());
				
				if (promocaoResultDTO.isNotIntegrated()) {
					idItemPromo = idItemPromo.add(BigDecimal.ONE); //Prepare next PK
					promocaoResultDTO.setIdItem(idItemPromo);
					promocaoResultDTO.setStatus(BigDecimal.ONE);
					promocaoResultDTO.feedParams(queInsereItemPromo);
					
					BigDecimal idProdPromo = BigDecimal.ZERO;
					for (PromocoesProdutoDTO promocoesProdutoDTO : promocaoResultDTO.getDetalhes().getProdutos()) {
						idProdPromo = idProdPromo.add(BigDecimal.ONE);
						
						promocoesProdutoDTO.setCodEmp(promocaoResultDTO.getCodEmp());
						promocoesProdutoDTO.setIdItem(idItemPromo);
						promocoesProdutoDTO.setIdProd(idProdPromo);
						
						promocoesProdutoDTO.feedParams(queInsereProdPromo);
						queInsereProdPromo.addBatch();
					}
					
					BigDecimal idBrindePromo = BigDecimal.ZERO;
					for (PromocoesProdutoDTO promocoesBrindeDTO : promocaoResultDTO.getDetalhes().getBrindes()) {
						idBrindePromo = idBrindePromo.add(BigDecimal.ONE);
						
						promocoesBrindeDTO.setCodEmp(promocaoResultDTO.getCodEmp());
						promocoesBrindeDTO.setIdItem(idItemPromo);
						promocoesBrindeDTO.setIdProd(idBrindePromo);
						
						promocoesBrindeDTO.feedParams(queInsereBrindePromo);
						queInsereBrindePromo.addBatch();
					}
					
					integratedPromocoes.add(promocaoResultDTO);
					queInsereItemPromo.addBatch();
				}
			}
			
			queInsereItemPromo.flushBatchTail();
			queInsereProdPromo.flushBatchTail();
			queInsereBrindePromo.flushBatchTail();
			
			try {
				new ProcessarDescontoService().processarPromocoes(integratedPromocoes);
			 	ctx.setMensagemRetorno(String.format("%d Promo��es encontradas e processadas!", integratedPromocoes.size()));
			} catch (Exception e) {
				ctx.setMensagemRetorno(e.getMessage());
			}
		} finally {
			NativeSql.releaseResources(queInsereItemPromo);
			NativeSql.releaseResources(queInsereProdPromo);
			NativeSql.releaseResources(queInsereBrindePromo);
		}
		
	}

}
