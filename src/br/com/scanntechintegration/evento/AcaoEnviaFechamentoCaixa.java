package br.com.scanntechintegration.evento;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Collection;

import br.com.sankhya.extensions.actionbutton.AcaoRotinaJava;
import br.com.sankhya.extensions.actionbutton.ContextoAcao;
import br.com.sankhya.extensions.actionbutton.Registro;
import br.com.sankhya.jape.EntityFacade;
import br.com.sankhya.jape.sql.NativeSql;
import br.com.sankhya.jape.util.FinderWrapper;
import br.com.sankhya.jape.vo.DynamicVO;
import br.com.sankhya.modelcore.util.DynamicEntityNames;
import br.com.sankhya.modelcore.util.EntityFacadeFactory;
import br.com.scanntechintegration.model.dto.CredentialsDTO;
import br.com.scanntechintegration.model.dto.FechaMovimentoDTO;
import br.com.scanntechintegration.service.EnviarMovimentoService;
import br.com.scanntechintegration.sql.SQLLoader;

public class AcaoEnviaFechamentoCaixa implements AcaoRotinaJava {

	@SuppressWarnings("unchecked")
	@Override
	public void doAction(ContextoAcao ctx) throws Exception {
		Registro[] registros = ctx.getLinhas();
		if (registros == null || registros.length == 0) {
			ctx.mostraErro("� necess�rio selecionar ao menos um caixa para envio!");
			return;
		}
		
		EntityFacade dwf = EntityFacadeFactory.getDWFFacade();
		EnviarMovimentoService enviarMovimentoService = new EnviarMovimentoService(new CredentialsDTO(ctx.getLinhaPai()));
		
		for (Registro registro : registros) {
			BigDecimal nuCaixa = (BigDecimal) registro.getCampo("NUCAIXA");
			StringBuilder criterio = new StringBuilder("this.NUNOTA IN ");
			criterio.append("(SELECT MCX.NROUNICO FROM TGFMCX MCX WHERE MCX.ORIGEM = 'E' AND MCX.NUCAIXA = ?) ");
			
			FinderWrapper finderWrapper = new FinderWrapper(DynamicEntityNames.CABECALHO_NOTA, criterio.toString(), new Object[] { nuCaixa });
			Collection<DynamicVO> cabVOs = dwf.findByDynamicFinderAsVO(finderWrapper);
			for (DynamicVO cabVO : cabVOs) {
				cabVO.setProperty("NROCAIXA", nuCaixa);
				enviarMovimentoService.adicionarNota(cabVO);	
			}
			
			NativeSql queGetDadosCaixa = null;
			ResultSet rs = null;
			try {
				queGetDadosCaixa = new NativeSql(dwf.getJdbcWrapper(), SQLLoader.class, "queGetDadosCaixa.sql");
				queGetDadosCaixa.setNamedParameter("NUCAIXA", nuCaixa);
				rs = queGetDadosCaixa.executeQuery();
				
				if (rs.next()) {
					FechaMovimentoDTO fechaMovimentoDTO = new FechaMovimentoDTO();
					fechaMovimentoDTO.setCodEmpresa((BigDecimal) registro.getCampo("CODEMP"));
					fechaMovimentoDTO.setNuCaixa(rs.getBigDecimal("NUCAIXA"));
					fechaMovimentoDTO.setDhCaixa(rs.getTimestamp("DTABERTURA"));
					fechaMovimentoDTO.setQtdVendaLiquida(rs.getBigDecimal("TOT"));
					fechaMovimentoDTO.setVlrVendaLiquida(rs.getBigDecimal("VLRLIQUIDO"));
					fechaMovimentoDTO.setQtdCancelamento(BigDecimal.ZERO);
					fechaMovimentoDTO.setVlrCancelamento(BigDecimal.ZERO);
					enviarMovimentoService.adicionarFechamento(fechaMovimentoDTO);
				}
			} finally {
				NativeSql.releaseResources(queGetDadosCaixa);
				try {
					rs.close();
				} catch (Exception ignored) {
				}
			}
		}
		
		enviarMovimentoService.start();
	}

}
